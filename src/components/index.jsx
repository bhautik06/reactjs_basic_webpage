import React, { Component } from "react";
import Description from "./desc/Description";
import Header from "./header/Header";
import OfflineTracking from "./offlineTracking/OfflineTracking";
import OnlineTracking from "./onlineTracking/OnlineTracking";
import Subscribe from "./subscribe/Subscribe";

class Index extends Component {
  render() {
    return (
      <>
        <Header />
        <OnlineTracking />
        <OfflineTracking />
        <Subscribe />
        <Description />
      </>
    );
  }
}

export default Index;
