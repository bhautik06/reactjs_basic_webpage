import React, { Component } from "react";
import "./header.css";

class Header extends Component {
  render() {
    return (
      <>
        <div className="headert_container">
          <header className="headers">
            <div className="header_img">
              <img
                src="/assets/header_logo.jpg"
                width="150px"
                height="140px"
                alt=""
              />
              <div className="header_label">
                <h3 className="label">Rebuilding The Earth</h3>
              </div>
              <div className="header_content">
                <div className="header_heading">
                  <span>
                    Your Journey From Reducing To Removing Carbon Emissions
                  </span>
                </div>
                <div className="header_text">
                  <p id="p1">
                    Evanzo is about moving a step closer towards a sustainable
                    lifestyle. We help you keep track of your carbon footprint
                    emissions in your daily life, from your everyday eating
                    habits to mobility choices, from lifestyle choices to
                    shopping habits.
                  </p>
                  <p id="p2">
                    Not just this, we help you in making better choices everyday
                    by recommending to you and providing you with alternatives
                    that you may adapt to for a more progressive sustainable
                    living, one step at a time. As you take on this journey, we
                    help make it easier by offering an e-shop for all your green
                    needs
                  </p>
                </div>
                <div>
                  <button id="btn1">sign up</button>
                  <span className="span">or</span>
                  <button id="btn2">shop now</button>
                </div>
              </div>
            </div>
            <div className="header_container">
              <img
                src="/assets/header_logo_2.png"
                width="800px"
                className="header_logo"
                alt=""
              />
            </div>
          </header>
        </div>
      </>
    );
  }
}

export default Header;
