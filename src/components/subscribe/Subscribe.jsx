import React, { Component } from "react";
import "./subscribe.css";

class Subscribe extends Component {
  render() {
    return (
      <>
        <div className="subscribe_container">
          <div className="sub_title">
            <span>You Choose, We Track.</span>
          </div>
          <div className="sub_title_2">
            <span>Every small step is one less carbon footprint…</span>
          </div>
          <div className="sub_form">
            <form>
              <div>
                <input type="text" placeholder="Your Email Address:" />
              </div>
              <div>
                <input type="text" placeholder="Contact Number:" />
              </div>
              <div>
                <button className="sub_btn">Subscribe</button>
              </div>
            </form>
            <p className="sub_footer">
              *By signing up you agree to the storage and publishing of the
              information provided by you, in our private database.
            </p>
          </div>
        </div>
      </>
    );
  }
}

export default Subscribe;
