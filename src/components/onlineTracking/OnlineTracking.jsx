import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import { MdSubscriptions } from "react-icons/md";
import { HiOutlineLightBulb } from "react-icons/hi";
import { VscMultipleWindows } from "react-icons/vsc";
import "./onlineTracking.css";
import "bootstrap/dist/css/bootstrap.min.css";

class OnlineTracking extends Component {
  render() {
    return (
      <>
        <div className="tracking_container">
          <div className="tracking_tag">
            <span className="tracking_headline">
              We help you help our planet
            </span>
          </div>
          <div className="tracking_label">
            <h3 className="label">How It Works</h3>
          </div>
          <div className="tracking_title">
            <span>Online Tracking</span>
          </div>
          <div className="card">
            <Row className="tracking_rows">
              <Col className="tracking_card">
                <span className="tracking_icon">
                  <MdSubscriptions />
                </span>
                <h4 className="tracking_title_2">subscibe</h4>
                <p>
                  Sign up to Evanzo with the email ID and contact number you use
                  for online shopping and food ordering.
                </p>
              </Col>
              <Col className="tracking_card">
                <span className="tracking_icon">
                  <HiOutlineLightBulb />
                </span>
                <h4 className="tracking_title_2">shop</h4>
                <p>
                  Verify your account and shop away. As you make purchases, your
                  activities help us work on understanding your needs and
                  everyday choices
                </p>
              </Col>
              <Col className="tracking_card">
                <span className="tracking_icon">
                  <VscMultipleWindows />
                </span>
                <h4 className="tracking_title_2">track</h4>
                <p>
                  Keep a track of your regular contribution to carbon emissions.
                  Follow customised recommendations as per your lifestyle
                  choices and win rewards as you progress.
                </p>
              </Col>
            </Row>
          </div>
        </div>
      </>
    );
  }
}

export default OnlineTracking;
