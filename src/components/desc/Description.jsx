import React, { Component } from "react";
import "./description.css";
import { Col, Row } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class Description extends Component {
  render() {
    return (
      <>
        <div className="desc_container">
          <Row>
            <Col sm={6} id="desc_col1">
              <div className="desc_content_1"></div>
              <div className="desc_content_2"></div>
            </Col>
            <Col sm={6} id="desc_col2">
              Hello2
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Description;
