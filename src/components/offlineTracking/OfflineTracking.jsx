import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import { MdSubscriptions } from "react-icons/md";
import { HiOutlineLightBulb } from "react-icons/hi";
import { VscMultipleWindows } from "react-icons/vsc";
import "./offlineTracking.css";
import "bootstrap/dist/css/bootstrap.min.css";

class OfflineTracking extends Component {
  render() {
    return (
      <>
        <div className="otracking_container">
          <div className="otracking_title">
            <span>Offline Tracking</span>
          </div>
          <div className="card">
            <Row className="otracking_rows">
              <Col className="otracking_card">
                <span className="otracking_icon">
                  <MdSubscriptions />
                </span>
                <h4 className="otracking_title_2">snap</h4>
                <p>
                  Click a picture of your offline earth-friendly purchases
                  through our Image Recognition Software.
                </p>
              </Col>
              <Col className="otracking_card">
                <span className="otracking_icon">
                  <HiOutlineLightBulb />
                </span>
                <h4 className="otracking_title_2">verify</h4>
                <p>
                  Verify the image details shown to you and Approve for us to
                  calculate your progress score.
                </p>
              </Col>
              <Col className="otracking_card">
                <span className="otracking_icon">
                  <VscMultipleWindows />
                </span>
                <h4 className="otracking_title_2">track</h4>
                <p>
                  Check the carbon emissions by your purchases and Win rewards
                  for your progress by making better choices through our
                  personalised recommendations.
                </p>
              </Col>
            </Row>
          </div>
        </div>
      </>
    );
  }
}

export default OfflineTracking;
